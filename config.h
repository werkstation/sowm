#ifndef CONFIG_H
#define CONFIG_H

#define MOD Mod1Mask

const char* menu[]    = {"/home/val/bin/menu", 0};
const char* term[]    = {"/usr/bin/urxvt", 0};
const char* voldown[] = {"amixer", "sset", "Master", "5%-",         0};
const char* volup[]   = {"amixer", "sset", "Master", "5%+",         0};
const char* volmute[] = {"amixer", "sset", "Master", "toggle",      0};
const char* bberry[]  = {"/usr/bin/blueberry", 0};

static struct key keys[] = {
    {MOD, XK_q,   win_kill,   {0}},
    {MOD, XK_c,   win_center, {.w = 0}},
    {MOD, XK_f,   win_fs,     {0}},
    {MOD, XK_Tab, win_next,   {0}},

    {MOD, XK_space,  run, {.com = menu}},
    {MOD, XK_Return, run, {.com = term}},
    {MOD, XK_b,  run, {.com = bberry}},
    {MOD, XK_j,  run, {.com = voldown}},
    {MOD, XK_k,  run, {.com = volup}},
    {MOD, XK_m,  run, {.com = volmute}},

    {MOD,           XK_1, ws_go,     {.i = 1}},
    {MOD|ShiftMask, XK_1, win_to_ws, {.i = 1}},
    {MOD,           XK_2, ws_go,     {.i = 2}},
    {MOD|ShiftMask, XK_2, win_to_ws, {.i = 2}},
    {MOD,           XK_3, ws_go,     {.i = 3}},
    {MOD|ShiftMask, XK_3, win_to_ws, {.i = 3}},
    {MOD,           XK_4, ws_go,     {.i = 4}},
    {MOD|ShiftMask, XK_4, win_to_ws, {.i = 4}},
};

#endif
